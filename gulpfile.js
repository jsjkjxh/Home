var gulp = require('gulp');
// 引入组件
var jshint = require('gulp-jshint');
var sass = require('gulp-sass');
var concat = require('gulp-concat');
var uglify = require('gulp-uglify');
var rename = require('gulp-rename');
var browserSync = require('browser-sync');

var reply = function(someone, content) {
    console.log('\n' + someone + '\n\t' + content + '\n');
}
var e_reply = reply.bind(this, '小e：');

gulp.task('browser', function() {
    var files = [
        '**/*.html',
        'css/**/*.css',
        'img/**/*.png',
        'js/**/*.js'
    ];

    browserSync.init(files, {
        server: {
            baseDir: './app',
            directory: true
        }
    });
});

// 检查脚本
gulp.task('lint', function() {
    e_reply('正在检查js，喝口水，放松放松。。。');

    gulp.src('./js/*.js')
        .pipe(jshint())
        .pipe(jshint.reporter('default'));
});

// 编译Sass
gulp.task('sass', function() {
    // src 可传入数组对象，各个匹配字符串
    e_reply('正在编译sass');

    gulp.src('./sass/*.scss')
        .pipe(sass())
        .pipe(gulp.dest('./css'));
});

// 合并，压缩文件
gulp.task('scripts', function() {
    e_reply('正在努力合并压缩中');

    gulp.src('./js/*.js')
        .pipe(concat('all.js'))
        .pipe(gulp.dest('./dist'))
        .pipe(rename('all.min.js'))
        .pipe(uglify())
        .pipe(gulp.dest('./dist'));
});

// 默认任务
gulp.task('default', function() {
    e_reply('让我看看哪里改变了，喝口茶，放松放松。。。');
    // gulp.run('lint', 'sass', 'scripts');
    // 监听文件变化
    gulp.watch(['./js/*.js', './sass/*.scss'], function() {
        gulp.run('lint', 'sass', 'scripts');
    });
});
