var $$ = function(id) {
    return document.getElementById(id);
};

/**
 * XHR
 * @return {[type]} [description]
 */
function createXHR() {
    if (window.XMLHttpRequest) {
        // 新浏览器
        return new XMLHttpRequest();
    } else if (window.ActiveXObject) {
        // IE5,IE6
        return new ActiveXObject("Microsoft.XMLHTTP");
    }
}

function xhrthen(xhr, callback) {
    xhr.onreadystatechange = function() {
        if (xhr.readyState == 4) {
            if (xhr.status >= 200 && xhr.status < 300 || xhr.status == 304) {
                console.log(xhr.responseText)
                callback(xhr.responseText);
            } else {
                console.log(xhr.status)
                console.log(xhr.responseText)
                callback(xhr.status, xhr.responseText);
            }
        }
    };
};

function log(content) {
    var args = Array.prototype.slice.call(arguments);
    args.unshift('[app]:');
    console.log.apply(console, args);
}


// (function() {

//     var xhr = createXHR();
//     var url = "http://123.206.203.36:8080/csrf-token";
//     xhr.open('get', url, false);
//     xhrthen(xhr, function(data) {
//         _TOKEN_ = data;
//         log(_TOKEN_)
//     })
//     xhr.send();

// })();

$$('user_signin').onsubmit = function(event) {
    event.preventDefault();
    console.log('ok')

    var url = "http://123.206.203.36:8080/auth/login";
    var xhr = createXHR();
    xhr.open('post', url, true);
    // xhr.setRequestHeader('X-CSRF-Token', _TOKEN_);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhrthen(xhr, function(data) {
        console.log(data)
    })
    var u_info = {
        username: 'asd',
        password: 'asd'
    }
    xhr.send(u_info);

}

$$('user_signup').onsubmit = function(event) {
    event.preventDefault();
    console.log('ok')
    var url = "http://123.206.203.36:8080/auth/register";
    var xhr = createXHR();
    xhr.open('post', url, false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhrthen(xhr, function(data) {
        console.log(data)
    });
    var data = {
        username: 'asd',
        password: 'asd'
    }
    xhr.send(data);
};

(function() {
    var url = "http://123.206.203.36:8080/auth/register";
    var xhr = createXHR();
    xhr.open('post', url, false);
    xhr.setRequestHeader("Accept", "application/json");
    xhr.setRequestHeader("Content-Type", "application/x-www-form-urlencoded");
    xhrthen(xhr, function(data) {
        console.log(data)
    });
    var data = {
        username: 'asd',
        password: 'asd'
    }
    xhr.send(data);
})();
