'use strict';
var dots = 4;
var sliderElem = document.querySelector('.slider');
var dotElems = sliderElem.querySelectorAll('.slider__dot');
var indicatorElem = sliderElem.querySelector('.slider__indicator');
var slider_slides=document.querySelectorAll('.slider__slide');
function Timer(){
	var currentPos = parseInt(sliderElem.getAttribute('data-pos'));
	var newDirection = dots > currentPos ? 'right' : 'left';
	var currentDirection = dots < currentPos ? 'right' : 'left';
	indicatorElem.classList.remove('slider__indicator--' + currentDirection);
	indicatorElem.classList.add('slider__indicator--' + newDirection);
	sliderElem.setAttribute('data-pos', (currentPos+1)%dots);
}
var timer=setInterval(Timer,3000);
// Array.prototype.forEach.call(slider_slides, function (slide) {
// 	slide.onmouseover=function(){
// 		clearInterval(timer);
// 	};
// 	slide.onmouseout=function(){
// 		timer=setInterval(Timer,3000);
// 	};
// });
Array.prototype.forEach.call(dotElems, function (dotElem) {
	dotElem.addEventListener('click', function (e) {
		var currentPos = parseInt(sliderElem.getAttribute('data-pos'));
		var newPos = parseInt(dotElem.getAttribute('data-pos'));
		var newDirection = newPos > currentPos ? 'right' : 'left';
		var currentDirection = newPos < currentPos ? 'right' : 'left';

		indicatorElem.classList.remove('slider__indicator--' + currentDirection);
		indicatorElem.classList.add('slider__indicator--' + newDirection);
		sliderElem.setAttribute('data-pos', newPos);
	});
	dotElem.onmouseover=function(){
		clearInterval(timer);
	};
	dotElem.onmouseout=function(){
		timer=setInterval(Timer,3000);
	};
});