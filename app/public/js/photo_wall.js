$(document).ready(function() {
	//your url important
    var __YOUR_URL__ = "";
    //---------------------------------------------------------------------------无聊的分割线
    var page = 1;
    var i = 0;
    var key = 1; //锁  0/1
    var box = $(".box"); //获取box属性
    var boxWidth = box.eq(0).width(); //box宽度
    var num = Math.floor($(window).width() * 0.65 / boxWidth); //一行的照片数量

    $(window).on("load", function() {

        imgLocation(); //调用函数
        //var dataImg = { "data":[{"src":"1.jpg"},{"src":"wechat_qt_code.jpg"},{"src":"temp_03.jpg"},{"src":"temp_04.jpg"},{"src":"temp_05.jpg"}]};//自动加载的对象
        window.onscroll = function() { //onscrool是监测滚条的
            if (scrollside() && key == 1) {
                key = 0; //上锁
                $.ajax({
                    type: "get",
                    url: "__YOUR_URL__",
                    dataType: "json",
                    data: { page: page, numPag: num },
                    success: function(json) {

                        $.each(json, function(index, elem) {
                            console.log(elem.image_url);
                            var box = $("<div>").addClass("box").appendTo($("#container1")); //创建新的盒子加在container里
                            var content = $("<div>").addClass("style").appendTo(box); //创建盒子在box里
                            //var aa = $("<a>").appendTo(content);
                            //console.log("./image/"+$(elem).attr("src"));
                            //$("<img>").attr("src","./image/"+$(elem).attr("src")).appendTo(content);//加载对象的路径，可能这是配合php的地方
                            $("<img>").attr("src", elem.image_url).appendTo(content);


                        });
                        imgLocation(); //调用

                        key = 1; //解锁
                        i++;
                        page++;

                    },
                    error: function(data) {
                        console.log(data);
                        key = 1; //解锁
                        //$("#f_tips").css({"display":"block"});
                    }
                });
                console.log("iiiiiiiiiiiiiiiiiiiiiiiiiii" + i);
            }
        };
    });
});

function scrollside() { //监测高度的，当看到最后一张的一半时，自动加载data里面的东西
    // var elemW=$("#logo").height()
    // 			+$("#nav").height()
    // 			+$(".add").height();

    var box = $(".box"); //获取属性
    var windowHeight = $(window).height();
    var documentHeight = $(document).height();
    var scrollHeight = $(window).scrollTop();
    var lastboxHeight = box.last().get(0).offsetTop + Math.floor(box.last().height() / 2) - scrollHeight; //当前浏览条+最后那张照片的一半高度

    //console.log(box.first().offset().top);
    //console.log(box.last().offset().top-scrollHeight);
    console.log(windowHeight);
    console.log("+++++++++++" + lastboxHeight)
        //return (lastboxHeight < windowHeight)?true:false;//判断现在的距离是否小于设置的高度
    return (documentHeight - scrollHeight - windowHeight < windowHeight / 2) ? true : false;
}

function imgLocation() { //用于添加图片，形成瀑布流
    var box = $(".box"); //获取box属性
    var boxWidth = box.eq(0).width(); //box宽度
    var num = Math.floor($(window).width() * 0.65 / boxWidth); //一行的照片数量
    var boxArr = []; //数组，用于瀑布流

    box.each(function(index, elem) { //each=循环
        //console.log("~~~~"+num);
        var HH = $(".box:first").offset().top;
        //console.log("~~~"+HH);
        var boxHeight = box.eq(index).height() + HH; //获取一行中最短的照片高度
        var w_box = $(".box").eq(0).width();
        w_box = w_box * num;
        $(".center").css({ "width": w_box })
        if (index < num) {
            boxArr[index] = boxHeight;
            //console.log(boxHeight);
        } else {
            var minboxHeight = Math.min.apply(null, boxArr);
            // console.log("---"+minboxHeight);
            var minboxIndex = $.inArray(minboxHeight, boxArr);
            //console.log(minboxIndex);
            //console.log(elem);
            $(elem).css({ //第二行第一张开始的CSS布局
                "position": "absolute", //绝对定位
                "top": minboxHeight, //这张照片的top是放在上一行中，最短的图的下面
                "left": box.eq(minboxIndex).position().left //left同理
            });
            boxArr[minboxIndex] += box.eq(index).height(); //添加完后，要重新计算图片的长度，然后继续循环
        }
    });
}
