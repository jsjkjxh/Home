###gulp工作流


### 上一个图片与下一个盒子间存在空隙（9.6）
- vertical-align:bottom   （把元素的顶端与行中最低的元素的顶端对齐。）

### 图片不居中问题
- 让图片先充满整个盒子
- 一种是外层block盒子，包裹三个inline-block盒子，外层盒子设置一下text-align:center
- flex布局，能够更加好地适配不同屏幕